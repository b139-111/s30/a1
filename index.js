//npm init
//npm install express
//npm install nodemon // npm install -g nodemon
	//add script "start": "nodemon index.js"
//create file name .gitignore - to ignore not needed files /node_modules
//npx nodemon index.js or node index.js
// npm install mongoose
const express = require('express');
const app = express();
const PORT = 4000;
const mongoose = require('mongoose');

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//connect server to mongodb atlas
mongoose.connect('mongodb+srv://Keanu_Orig:J3^bmsqa@batch139.1lth5.mongodb.net/user?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//get notification if successfully connected or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));

//Schema
const userSchema = new mongoose.Schema(
	{
		username: String,
		password: String
	}
);

//Models
const User = mongoose.model("User", userSchema);

//ROUTES

/*Business Logic
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exists in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/signup", (req, res) => {
	 //console.log(req.body)

	let username = req.body.username;
	let password = req.body.password;

	User.create({username, password}, (err, result) => {
		console.log(result)
		if(result != null){
			return res.send(`username: ${req.body.username} and password: ${req.body.password}`)
			
		} 
		else {
			return res.send(`Not valid`)
		}
	});
})
app.listen(PORT, () => console.log(`Server running at port ${PORT}`));
